#!/bin/bash
# --------------------------------------------------

Greeting=$(date +%H)
cat $0 | grep $Greeting | sed 's/# '$Greeting' //'

# --------------------------------------------------
# 00 una buona Notte
# 01 una buona Mattinata
# 02 una buona Mattinata
# 03 una buona Mattinata
# 04 una buona Mattinata
# 05 una buona Mattinata
# 06 una buona Mattinata
# 07 una buona Mattinata
# 08 una buona Mattinata
# 09 una buona Mattinata
# 10 una buona Mattinata
# 11 un buon Mezzogiorno
# 12 un buon Mezzogiorno
# 13 un buon Pomeriggio
# 14 un buon Pomeriggio
# 15 un buon Pomeriggio
# 16 un buon Pomeriggio
# 17 un buon Pomeriggio
# 18 una buona Serata
# 19 una buona Serata
# 20 una buona Serata
# 21 una buona Serata
# 22 una buona Serata
# 23 una buona Serata
