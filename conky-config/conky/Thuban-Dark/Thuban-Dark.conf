conky.config = {
--==============================================================================

--  This theme is for conky version 1.10.8 or newer
--
--  THUBAN DARK
--  ( A part of Draco Conky themes pack )
--
--  Original author : Closebox73
--  Created         : 2023/09/15
--  Variant         : Playerctl & Celsius
--  Modified by     : TheGreatYellow67
--  Modified date   : 2024/02/18
--  Variant         : Italian language
--
--  License         : Distributed under the terms of GPLv3
--  Notes           : Created on 1920x1080 Monitor

--==============================================================================

-- Size and Position settings --
  alignment = 'middle_right',
  gap_x = 30,
  gap_y = 0,
  maximum_width = 350,
  minimum_height = 700,
  minimum_width = 350,
  
-- Text settings --
  use_xft = true,
  override_utf8_locale = true,
  font = 'Lato:size=9',
  
-- Color Settings --
  default_color = 'white',
  default_outline_color = 'white',
  default_shade_color = 'white',
  color1 = '#C50ED2',
  color2 = '#197FDA',
  color3 = '#4A9057',
  
-- Window Settings --
  background = false,
  --draw_blended = false,
  border_width = 1,
  draw_borders = false,
  draw_graph_borders = false,
  draw_outline = false,
  draw_shades = false,
  own_window = true,
  own_window_class = 'Conky',
  stippled_borders = 0,

-- To get REAL TRANSPARENCY --
  own_window_type = 'desktop',
  own_window_transparent = false,
  own_window_hints = 'undecorated,sticky,skip_taskbar,skip_pager,below',
  own_window_colour = '#000000',
  own_window_argb_visual = true,
  own_window_argb_value = 0,
  
-- Others --
  cpu_avg_samples = 2,
  net_avg_samples = 1,
  double_buffer = true,
  out_to_console = false,
  out_to_stderr = false,
  extra_newline = false,
  update_interval = 1,
  uppercase = false,
  use_spacer = 'none',
  show_graph_scale = false,
  show_graph_range = false,
  lua_load = '~/.config/conky/Thuban-Dark/scripts/rings-v1.2.1.lua',
  lua_draw_hook_pre = 'ring_stats',

-- Templates --
  template1 = "wlan0",
  template2 = "enp0s3",
}

conky.text = [[
##
## Script per il meteo ##
##
${execi 600 ~/.config/conky/Thuban-Dark/scripts/weather-v2.0.sh}\
##
## Immagini ##
##
${image ~/.config/conky/Thuban-Dark/res/symbols.png -p 0,215 -s 350x70}\
${image ~/.config/conky/Thuban-Dark/res/separator.png -p 140,585 -s 200x1}\
${image ~/.config/conky/Thuban-Dark/res/graphics-background.png -p 11,313 -s 160x30}\
${image ~/.config/conky/Thuban-Dark/res/graphics-background.png -p 180,313 -s 160x30}\
##
## Ora / Data ##
##
${alignr}${font Bebas Neue:size=60}${color1}${time %H}${color2}:${time %M}${font}
${offset 158}${voffset -47}${font Bebas Neue:bold:size=22}${time %d}${font}
${offset 158}${voffset -45}${font Bebas Neue:bold:size=14}${color1}${time %b}${font}
${offset 158}${voffset 22}${font Bebas Neue:bold:size=14}${color3}${time %Y}${font}
${alignr 15}${voffset 5}${font Bebas Neue:bold:size=18}${color2}${time %A}${font}
##
## Info meteo ##
##
${voffset -5}${offset 190}${color}${font feather:size=45}${execi 15 ~/.config/conky/Thuban-Dark/scripts/weather-text-icon.sh}${voffset -4}${offset 8}${font Bebas Neue:size=35}${execi 100 cat ~/.cache/weather.json | jq '.main.temp' | awk '{print int($1+0.5)}'}°C${font}
${alignr 15}${voffset 10}${color}${font Lato:size=12}${execi 100 cat ~/.cache/weather.json | jq -r '.name'} (${execi 100 cat ~/.cache/weather.json | jq -r '.sys.country'}), ${execi 100 ~/.config/conky/Thuban-Dark/scripts/weather-description.sh}${font}
${alignr 15}${voffset 0}${color}${font Lato:size=12}Umidità ${execi 100 cat ~/.cache/weather.json | jq '.main.humidity'}%, vento a ${execi 100 cat ~/.cache/weather.json | jq '.wind.speed'} km/h
##
## Info rete ##
##
${offset 10}${voffset 85}${color}${font Lato:size=10}${if_empty ${exec ip -br l | awk '$1 !~ "lo|vir|en" { print $1}'}}Giù : ${color2}${downspeed ${template2}}${goto 185}${color}Su : ${color2}${upspeed ${template2}}${color}${else}Giù : ${color2}${downspeed ${template1}}${goto 185}${color}Su : ${color2}${upspeed ${template1}}${color}${endif}
${voffset -2}${offset 10}${color1}${if_empty ${exec ip -br l | awk '$1 !~ "lo|vir|en" { print $1}'}}${upspeedgraph ${template2} 30,160}${offset 10}${downspeedgraph ${template2} 30,160}${else}${upspeedgraph ${template1} 30,160}${offset 10}${downspeedgraph ${template1} 30,160}${endif}${color}
${alignr -4}${voffset -2}${font Lato:size=12}Lan : ${color2}${execi 3 ~/.config/conky/Thuban-Dark/scripts/lan.sh}${color} | Wifi : ${color2}${execi 3 ~/.config/conky/Thuban-Dark/scripts/ssid.sh}${color}
##
## Info sistema ##
##
${alignr 10}${voffset 2}${color}${font Lato:size=12}Utente : ${color2}${execi 800 whoami}
${alignr 10}${voffset 2}${color}${font Lato:size=12}Distro : ${color2}${execi 1200 lsb_release -sd}
${alignr 10}${voffset 2}${color}${font Lato:size=12}Attivo da : ${color2}${uptime}
##
## Info musicali ##
##
${alignr 10}${voffset 8}${color1}${font Bebas Neue:size=18}${execi 3 ~/.config/conky/Thuban-Dark/scripts/playerctl-status.sh}
${alignr 10}${voffset -6}${font Lato:size=12}${color2}${execi 3 ~/.config/conky/Thuban-Dark/scripts/playerctl-artist.sh}
${alignr 10}${voffset 2}${font Lato:italic:size=12}${color2}${execi 3 ~/.config/conky/Thuban-Dark/scripts/playerctl-title.sh}${color}
##
## Spazio di lavoro ##
##
${alignr -2}${voffset 8}${color1}${font Bebas Neue:size=18}Spazio di lavoro ${color}(${color1}${execi 1 wmctrl -d | grep -w '*' | awk '{print $10}'}${color})
${alignr -2}${voffset -4}${font PizzaDude Bullets:pixelsize=14:weight=bold}\
${if_match $desktop==1}${color2}8${color} 8 8 8${endif}\
${if_match $desktop==2}8 ${color2}8${color} 8 8${endif}\
${if_match $desktop==3}8 8 ${color2}8${color} 8${endif}\
${if_match $desktop==4}8 8 8 ${color2}8${color}${endif}
##
## Info su Conky Thuban ##
##
${alignr 10}${voffset 13}${color2}${font Lato:bold:size=12}Conky Thuban Dark${color}
${alignr 10}${voffset 2}${color1}${font Lato:size=10}Creato il: 01/03/2024
]]
